[Unit]
Description=PRTG Miniprobe Service
After=network.target

[Service]
Type=simple
Restart=always
RestartSec=1
User=%s
ExecStart=/usr/bin/python -m miniprobe.probe

[Install]
WantedBy=multi-user.target
