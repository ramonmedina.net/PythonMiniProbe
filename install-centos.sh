#!/bin/bash

DOWNLOADED=false
if [ "$(id -u)" != "0" ]; then
        echo "Sorry, you are not root."
        exit 1
fi

echo "This script will guide you to install the PythonMiniProbe"

echo "installing python-dev and build-essentials"
yum groupinstall "Development Tools" 2>&1 | tee -a /tmp/probe_install.log
yum install python-devel build-essential 2>&1 | tee -a  /tmp/probe_install.log


case "$(python --version 2>&1)" in
    *" 2.7.9"*)
        echo "Correct python version!"
        ;;
    *" 3."*)
        echo "Correct python version!"
        ;;
    *)
        echo "Installing PIP!"
        yum install python2-pip 2>&1 | tee -a /tmp/probe_install.log
        ;;
esac

if [ ! -f ./README.md ]
then
    read -p "Use git to install the miniprobe (y|n)? " -n 1 -r
    echo    # (optional) move to a new line
    if [[ $REPLY =~ ^[Yy]$ ]]
    then
        git clone https://gitlab.com/ramonmedina.net/PythonMiniProbe.git /PythonMiniProbe
        cd /PythonMiniProbe
        DOWNLOADED=true
    else
        read -p "Use wget to install the miniprobe (y|n)? " -n 1 -r
        echo    # (optional) move to a new line
        if [[ $REPLY =~ ^[Yy]$ ]]
        then
            wget -O /tmp/probe.zip https://gitlab.com/ramonmedina.net/PythonMiniProbe/-/archive/master/PythonMiniProbe-master.zip
            unzip /tmp/probe.zip -d /tmp
            mv /tmp/PythonMiniProbe-master /PythonMiniProbe
            cd /PythonMiniProbe
            DOWNLOADED=true
        fi
    fi
else
    DOWNLOADED=true
fi

if [ "$DOWNLOADED" = true ]
then
    echo "Starting to install the miniprobe and requirements"
    python setup-centos.py build
    python setup-centos.py configure
    python setup-centos.py install
fi

